/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mr.obj;

/**
 *
 * @author rgallo
 */
public class Coordinate {

    private int x = 0;
    private int y = 0;

    public Coordinate() {
    }
 
    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coordinate(Coordinate coordinate) {
        this.x = coordinate.x;
        this.y = coordinate.y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void add(Coordinate coordinate) {
        this.x = this.x + coordinate.getX();
        this.y = this.y + coordinate.getY();

    }

    public void scale(int m) {
        this.x *= m;
        this.y *= m;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    public static Coordinate add(Coordinate from, Coordinate to) {
        Coordinate result = new Coordinate(from);
        result.add(to);
        
        return result;
    }
}

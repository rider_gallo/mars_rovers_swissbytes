/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mr.obj;

/**
 *
 * @author rgallo
 */
public enum Position {

    /**
     * NORTH
     */
    N(0, 1),
    /**
     * EAST
     */
    E(1, 0),
    /**
     * SOUTH
     */
    S(0, -1),
    /**
     * WEST
     */
    W(-1, 0);

    private final Coordinate coordinate;

    Position(int x, int y) {
        coordinate = new Coordinate(x, y);
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Position left() {
        //return the left (i.e. anti clockwise) move of 'this' direction
        switch (this) {
            case N:
                return W;
            case E:
                return N;
            case S:
                return E;
            case W:
                return S;
        }

        throw new AssertionError("Unknown direction: " + this);
    }

    public Position right() {
        //return the right (i.e. clockwise) move of 'this' direction
        switch (this) {
            case N:
                return E;
            case E:
                return S;
            case S:
                return W;
            case W:
                return N;
        }

        throw new AssertionError("Unknown direction: " + this);
    }

}

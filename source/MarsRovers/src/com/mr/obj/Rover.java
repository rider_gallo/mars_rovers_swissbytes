/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mr.obj;

/**
 *
 * @author rgallo
 */
public class Rover {

    private Coordinate coordinate;
    private Position position;

    private boolean out = Boolean.getBoolean("false");

    public Rover() {
    }

    public Rover(int x, int y, Position pos) {
        coordinate = new Coordinate(x, y);
        this.position = pos;
    }
    
    public Rover(int x, int y, Position pos, boolean out) {
        coordinate = new Coordinate(x, y);
        this.position = pos;
        this.out = out;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate xy) {
        this.coordinate = xy;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position pos) {
        this.position = pos;
    }

    public void move() {
        coordinate.add(position.getCoordinate());
    }

    public void moveBy(int m) {
        Coordinate data = new Coordinate(position.getCoordinate());
        data.scale(m);
        coordinate.add(data);
    }

    public void left() {
        this.position = this.position.left();
    }

    public void right() {
        this.position = this.position.right();
    }

    /*
     * Heart of the system
     * Navigates based on plan (string of instruction characters) and
     * a map that defines the boundaries
     * So...
     * The rover knows where it is going
     *
     * TODO implement logic to avoid collision with other rovers
     */
    public void navigate(String plan, Map map) {
        //must check if boundaries are crossed
        //rover must have knowledge of planet (map)

        char[] instructions = plan.toCharArray();
        char ins = 0;

        int moveX = 0;
        int moveY = 0;
        Coordinate moveTo = null;

        outPut("Start navigate Rover...");
        outPut(this);

        for (int i = 0; i < instructions.length; i++) {
            outPut(instructions[i]);
            switch (instructions[i]) {
                case 'L':
                    left();
                    break;
                case 'R':
                    right();
                    break;
                case 'M':
                    //check if it's a valid move
                    //a move is invalid if it takes the rover beyond the boundary

                    //find out what the final destination will be
                    moveTo = Coordinate.add(this.coordinate, this.position.getCoordinate());

                    if (map.onMap(moveTo)) {
                        //safe move as it doesn't take us out of the boundaries
                        move();
                    } else {
                        //don't move
                        //should we throw an exception?
                        outPut("invalid move");
                    }
                    break;

                default: //do nothing; we just ignore all crappy instructions.
            }

            outPut(this);
        }

        outPut("End navigate Rover.");
        System.out.println(this);
    }

    @Override
    public String toString() {
        return coordinate.toString() + "  " + position.toString();
    }

    private void outPut(Object o) {
        if (out) {
            System.out.println(o);
        }
    }
}

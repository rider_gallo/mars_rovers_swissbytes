/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mr.obj;

/**
 *
 * @author rgallo
 */
public class Map {

    private Coordinate left = new Coordinate(0, 0);
    private Coordinate right = new Coordinate(0, 0);

    private int maxX = right.getX();
    private int maxY = right.getY();

    public Map() {
    }

    public Map(Coordinate right) {
        this.right = new Coordinate(right);

        maxX = right.getX();
        maxY = right.getY();
    }

    public int getMaxX() {
        return maxX;
    }

    public int getMaxY() {
        return maxY;
    }

    public void setBoundary(Coordinate right) {
        this.right = new Coordinate(right);
        maxX = right.getX();
        maxY = right.getY();
    }

    public boolean onMap(Coordinate xy) {
        if ((0 <= xy.getX() && xy.getX() <= maxX)
                && (0 <= xy.getY() && xy.getY() <= maxY)) {
            return true;
        } else {
            return false;
        }
    }
}

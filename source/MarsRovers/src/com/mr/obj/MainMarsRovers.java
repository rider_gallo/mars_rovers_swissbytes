/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mr.obj;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author rgallo
 */
public class MainMarsRovers {

    public static void main(String[] args) {
        System.exit(navigate(args));
    }

    public static int navigate(String[] args) {
        System.out.println("MARS ROVERS...");
        System.out.println("Begin Program...");

        try {
            Map map = new Map();

            String fileName = args[0]; //do basic authentication later
            Boolean out = Boolean.valueOf(args[1]);
            
            //read input data
            BufferedReader f = new BufferedReader(new FileReader(fileName));
            String line = null;

            //read first line
            line = f.readLine();
            if (line != null) {
                //top right coordinates
                line = line.trim();
                int x = Integer.parseInt(line.substring(0, 1));
                int y = Integer.parseInt(line.substring(2));
                map.setBoundary(new Coordinate(x, y));
            }

            int i = 1;
            Rover r = null;
            //TODO should we maintain a list of rovers
            while ((line = f.readLine()) != null) {
                line = line.trim();
                if (i % 2 != 0) {
                    // first line of input for rover
                    int x = Integer.parseInt(line.substring(0, 1));
                    int y = Integer.parseInt(line.substring(2, 3));

                    char d = line.charAt(4);
                    switch (d) {
                        case 'N':
                            r = new Rover(x, y, Position.N, out);
                            break;
                        case 'E':
                            r = new Rover(x, y, Position.E, out);
                            break;
                        case 'S':
                            r = new Rover(x, y, Position.S, out);
                            break;
                        case 'W':
                            r = new Rover(x, y, Position.W, out);
                            break;
                        default:
                            r = new Rover(x, y, Position.N, out);
                            break; //create a new Rover anyway
                    }

                } else {
                    // second line of input
                    //TODO implement logic to avoid collision with other rovers
                    r.navigate(line, map);
                }
                i++;
            }

        } catch (IOException | NumberFormatException e) {
            return (-1);
        }

        System.out.println("End Program.");
        return (0);
    }
}

Resolucion de problema Mars Rovers

-documents: Especificacion del problema
-delivery: Contiene el archivo ejecutable (.jar) con las instrucciones para ejecutar la aplicacion
-source: Codigo fuente de la aplicacion realizada en NetBeans 7.4 y JDK 1.7
 Para ejecutar la aplicacion desde el IDE:
 1.-Se debe copiar el archivo data.txt que contiene la entrada de datos que se encuentra en la raiz del proyecto a la carpeta dist.
 2.-Asignar los argumentos de entrada: Sobre el proyecto click derecho->Properties->Run->Arguments escribir data.txt false

Todos estos documentos se encuentran en bitbucket en el siguiente repositorio:
https://rider_gallo@bitbucket.org/rider_gallo/mars_rovers_swissbytes.git
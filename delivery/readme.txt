1.-Requisitos:
	Tener instalado el JRE
2.-Ejecutar el siguiente comando en linea de comando/consola 	
	java -jar MarsRovers.jar FILE_NAME FLAG_OUT_NAVIGATE
	donde:
	FILE_NAME Nombre del archivo de entrada de datos del MARS ROVER, el archivo tiene que ser un archivo texto plano txt
	FLAG_OUT_NAVIGATE Bandera para indicar si se imprime la navagacion: true imprimir navegacion, false no se imprime la navegacion
	
3.-Ejemplo de ejecucion
	Comando:
	java -jar MarsRovers.jar data.txt false
	salida
	MARS ROVERS...
	Begin Program...
	(1,3)  N
	(5,1)  E
	End Program.
	
	Comando:
	java -jar MarsRovers.jar data.txt true
	Salida:
	MARS ROVERS...
	Begin Program...
	Start navigate Rover...
	(1,2)  N
	L
	(1,2)  W
	M
	(0,2)  W
	L
	(0,2)  S
	M
	(0,1)  S
	L
	(0,1)  E
	M
	(1,1)  E
	L
	(1,1)  N
	M
	(1,2)  N
	M
	(1,3)  N
	End navigate Rover.
	(1,3)  N
	Start navigate Rover...
	(3,3)  E
	M
	(4,3)  E
	M
	(5,3)  E
	R
	(5,3)  S
	M
	(5,2)  S
	M
	(5,1)  S
	R
	(5,1)  W
	M
	(4,1)  W
	R
	(4,1)  N
	R
	(4,1)  E
	M
	(5,1)  E
	End navigate Rover.
	(5,1)  E
	End Program.
